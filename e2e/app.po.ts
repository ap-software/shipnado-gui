import { browser, by, element } from 'protractor';

export class ShipnadoGuiPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('app-root h1')).getText();
  }
}
