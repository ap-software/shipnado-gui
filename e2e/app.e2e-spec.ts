import { ShipnadoGuiPage } from './app.po';

describe('shipnado-gui App', () => {
  let page: ShipnadoGuiPage;

  beforeEach(() => {
    page = new ShipnadoGuiPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
