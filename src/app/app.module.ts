import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { WelcomeSiteComponent } from './components/welcome-site/welcome-site.component';
import { ScrollSpyForDirective } from './directives/app-scroll-spy/scroll-spy-for.directive';
import { LogInComponent } from './components/log-in/log-in.component';
import { MockUserService } from './mockes/services/mock-user.service';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeSiteComponent,
    ScrollSpyForDirective,
    LogInComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [
    {provide: 'UserService', useClass: MockUserService}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
