export class User {
  private _username: string;

  constructor(username: string) {
    this._username = username;
  }

 public get username(){
    return this._username;
  }

 public set username(username: string){
    this._username = username;
  }
}
