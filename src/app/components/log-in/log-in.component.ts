import { UserService } from '../../services/user.service';
import { Component, OnInit, Inject } from '@angular/core';
import { User } from '../../utilities/user';
import { NgForm } from '@angular/forms';

const USERNAME_MIN_LENGTH = 3;
const USERNAME_MAX_LENGTH = 50;
const SEARCH_DELAY = 2000;

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.css']
})
export class LogInComponent implements OnInit {


  user: User = new User('');
  usernameFeedback = '';
  usernameValid = false;
  isWaitingForUsernameConfirmation = false;
  lastConfirmedUsername = '';
  confirmUsernameTimeout = undefined;

  constructor(
    @Inject('UserService') private userService: UserService
  ) {}

  ngOnInit() {
  }

  onKey(value: string) {
    if (value.length === 0) {
      this.usernameFeedback = 'Username is required';
      this.usernameValid = false;
    } else if (value.length < USERNAME_MIN_LENGTH) {
      this.usernameFeedback = 'Username min. length: ' + USERNAME_MIN_LENGTH;
      this.usernameValid = false;
    } else if (value.length > USERNAME_MAX_LENGTH) {
      this.usernameFeedback = 'Username max. length: ' + USERNAME_MAX_LENGTH;
      this.usernameValid = false;
    } else if (value.match('[^a-z0-9]')) {
      this.usernameFeedback = 'Username can contain only small letters [a-z] and numbers [0-9]';
      this.usernameValid = false;
    } else {
      if (this.lastConfirmedUsername !== value) {
          this.usernameValid = true;
          if ((this.confirmUsernameTimeout !== undefined) || (this.confirmUsernameTimeout !== null)) {
            clearTimeout(this.confirmUsernameTimeout);
        }
        this.confirmUsernameTimeout = setTimeout(() => this.confirmUsername(value), SEARCH_DELAY);
      }
    }
  }

  private confirmUsername(username) {
    console.log('User exists: ' + username);
    this.lastConfirmedUsername = username;
    this. isWaitingForUsernameConfirmation = true;
    this.userService.findUser(username).subscribe(
      data => this.onConfirmUsernameSuccess(data),
      error => this.onConfirmUsernameError(error),
      () => this.onConfirmUsernameComplete()
    );
  }

  private onConfirmUsernameSuccess(data: boolean) {
    if (data === true) {
      // console.log('User exists');
      this.usernameFeedback = 'Username already exists';
      this.usernameValid = false;
    } else {
      // console.log('User doesn\'t exist');
      this.usernameValid = true;
      this.usernameFeedback =  '';
    }
  }

  private onConfirmUsernameError(error: any) {
      console.log('Error: ' + error);
  }

  private onConfirmUsernameComplete() {
    this.isWaitingForUsernameConfirmation = false;
     // console.log('ConfirmUsername completed');
  }

  private fetchlistOfUsers() {
    this.userService.listOfUsers().subscribe(
      items => console.log('SUCCESS' + items),
      error => console.log('ERROR'),
      () => console.log('COMPLETE')
    );
  }

}
