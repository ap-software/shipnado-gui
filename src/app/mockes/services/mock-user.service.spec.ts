import { TestBed, inject } from '@angular/core/testing';

import { MockUserService } from './user.service';

describe('MockUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UserService]
    });
  });

  it('should be created', inject([MockUserService], (service: MockUserService) => {
    expect(service).toBeTruthy();
  }));
});
