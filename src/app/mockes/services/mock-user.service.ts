import { UserService } from '../../services/user.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/delay';

const USERNAMES: string[]  = ['ala', 'adam', 'artur', 'rafal'];
const DELAY = 3000;

@Injectable()
export class MockUserService extends UserService {


  listOfUsers(): Observable<string[]> {
    return Observable.of(USERNAMES).delay(DELAY);
  }

  findUser(username: string): Observable<boolean> {
    if (USERNAMES.find((item) => item === username)) {
      return Observable.from([true]).delay(DELAY);
    }
    return Observable.from([false]).delay(DELAY);
  }
}
