import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

export abstract class UserService {

  abstract listOfUsers(): Observable<string[]>;
  abstract findUser(username: string): Observable<boolean>;
}
