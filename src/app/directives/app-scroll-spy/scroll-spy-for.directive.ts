import { Directive, ElementRef, Input , Renderer, HostListener, Inject} from '@angular/core';
import { DOCUMENT } from '@angular/common';


const offsetMax = 200;
const offsetMin = -200;

@Directive ({
  selector: '[appScrollSpyFor]'
})
export class ScrollSpyForDirective  {
  @Input() appScrollSpyFor;
  constructor(
     @Inject(DOCUMENT) private document: Document,
     private el: ElementRef,
     private renderer: Renderer) { }


  @HostListener('window:scroll', ['$event']) onWindowScroll(event: Event) {
      const position = document.getElementById(this.appScrollSpyFor).getBoundingClientRect().top;
      if (position <= offsetMax && position >= offsetMin) {
        const tmp: any = Array.prototype.slice.call(document.getElementsByClassName('active'));
        tmp.map(currentItem => {
          this.renderer.setElementClass(currentItem, 'active', false);
        });
        this.renderer.setElementClass(this.el.nativeElement, 'active', true);
      }
   }


}
